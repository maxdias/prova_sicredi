# Prova Sicredi
> Desafio 1 -> Cadastro de um registro.
> Desafio 2 -> Delete de um registro.

[![NPM Version][7.19.1]][npm install node@7.19.1]
[![Cypress][10.3.0]][npm instal cypress@10.3.0]

Prova Sicredi para automação de duas telas com as seguintes implemnetações:
> Desafio 1 -> Cadastro de um registro.
> Desafio 2 -> Delete de um registro.
> Durante o processo de execução da automação foi realizado o print de evidências que são salvos na pasta screenshot

![](../header.png)

## Instalação

OS X:
Node: https://nodejs.org/dist/v16.15.1/node-v16.15.1.pkg
Linux: https://nodejs.org/dist/v16.15.1/node-v16.15.1-linux-ppc64le.tar.xz
Windows: https://nodejs.org/dist/v16.15.1/node-v16.15.1-x64.msi


## Plugins e Dependências do projeto

Utilizar terminal:

npm install chai
npm install cypress-localstorage-commands
npm install cypress-pipe
npm install cypress-plugin-tab
npm install cypress-wait-until
npm install mocha
npm install node
npm install pg
npm install po
npm install sequelize
npm install shadow
npm install toast
npm install xpath
npm install cypress
npm install cypress-image-snapshot
npm install cypress-xpath


## Configuração para Desenvolvimento

PS C:\..\..\..\Projetos\prova_sicredi> code .
PS C:\..\..\..\Projetos\\prova_sicredi> npm init -y
Wrote to C:\..\..\..\Projetos\\prova_sicredi\package.json:

{
  "name": "prova_sicredi",
  "version": "1.0.0",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "cypress:open": "cypress open"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "chai": "^4.3.6",
    "check": "^1.0.0",
    "cypress": "^10.3.0",
    "cypress-pipe": "^2.0.0",
    "cypress-plugin-tab": "^1.0.5",
    "cypress-wait-until": "^1.7.2",
    "cypress-xpath": "^2.0.0",
    "faker": "^6.6.6",
    "focused": "^0.7.2",
    "mocha": "^10.0.0",
    "node": "^18.4.0",
    "sequelize": "^6.21.2",
    "toast": "^0.3.47",
    "xpath": "^0.0.32"
  },
  "devDependencies": {},
  "description": ""
}

PS \C:\..\..\..\Projetos\\prova_sicredi> npm run cypress:open

## Histórico de lançamentos

* 0.0.1
    * Prova Sicredi

## Meta

Max Dias – [@maxdias] – maxdias@imaxsi.com

[https://gitlab.com/maxdias/prova_sicredi]