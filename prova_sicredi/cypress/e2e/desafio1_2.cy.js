///<reference types="Cypress" />

//Desafio 1 e 2
describe('Prova Sicredi', () => {

  before(() => {});
  beforeEach(() => {

    //1.	Acessar a página https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap
    cy.log('1.	Acessar a página https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap')
    cy.visit('https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap')

  })
    
    it('Desafio 1 e 2', () => {

      cy.log('***Início do Desafio 1***')
      //2.	Mudar o valor da combo Select version para “Bootstrap V4 Theme”
      cy.log("2.	Mudar o valor da combo Select version para “Bootstrap V4 Theme”")
      cy.get('#switch-version-select')
        .select('Bootstrap V4 Theme')
      
      //3. Clicar no botão Add Customer
      cy.log("3. Clicar no botão Add Customer")
      cy.xpath('//*[@id="gcrud-search-form"]/div[1]/div[1]/a')
        .click({force:true})

      //4. Preencher os campos do formulário com as seguintes informações:
      //Name: Teste Sicredi
      cy.log('Name: Teste Sicredi')
      cy.get('#field-customerName')
        .clear()
        .click({force:true})
        .type('Teste Sicredi')

      //Last name: Teste
      cy.log('Last name: Teste')
      cy.get('#field-contactLastName')
        .clear()
        .click({force:true})
        .type('Teste')

      //ContactFirstName: seu nome
      cy.log('ContactFirstName: seu nome')
      cy.get('#field-contactFirstName')
        .clear()
        .click({force:true})
        .type('Maxwel Dias')

      //Phone: 51 9999-9999
      cy.log('Phone: 51 9999-9999')
      cy.get('#field-phone')
        .clear()
        .click({force:true})
        .type('51 9999-9999')

      //AddressLine1: Av Assis Brasil, 3970
      cy.log('AddressLine1: Av Assis Brasil, 3970')
      cy.get('#field-addressLine1')
        .clear()
        .click({force:true})
        .type('Av Assis Brasil, 3970')

      //AddressLine2: Torre D
      cy.log('AddressLine2: Torre D')
      cy.get('#field-addressLine2')
        .clear()
        .click({force:true})
        .type('Torre D')

      //City: Porto Alegre
      cy.log('City: Porto Alegre')
      cy.get('#field-city')
        .click({force:true})
        .clear()
        .type('Porto Alegre')

      //State: RS
      cy.log('State: RS')
      cy.get('#field-state')
        .clear()
        .click({force:true})
        .type('RS')

      //PostalCode: 91000-000
      cy.log('PostalCode: 91000-000')
      cy.get('#field-postalCode')
        .clear()
        .click({force:true})
        .type('91000-000')

      //Country: Brasil
      cy.log('Country: Brasil')
      cy.get('#field-country')
        .clear()
        .click({force:true})
        .type('Brasil')

      /*
      //from Employeer: Fixter
      cy.log('from Employeer: Fixter')
      cy.get('#field-salesRepEmployeeNumber')
        .clear()
        .click({force:true})
        .type('Fixter')
      */

      //CreditLimit: 200
      cy.log('CreditLimit: 200')
      cy.get('#field-creditLimit')
        .clear()
        .click({force:true})
        .type('200')

      /*
      cy.log('')
      cy.get('#field-deleted')
        .clear()
        .click({force:true})
        .type('')
      */

      cy.screenshot('Campos cadastrados')

      //1. Clicar no botão Save
      cy.log('1.Clicar no botão Save')
      cy.get('#form-button-save')
        .click({force:true})

      //2.Validar a mensagem “Your data has been successfully stored into the database. Edit Customer or Go back to list” através de uma asserção
      cy.log('2.Validar a mensagem “Your data has been successfully stored into the database. Edit Customer or Go back to list” através de uma asserção')
      //Verifica mensagem de sucesso
      cy.contains('Your data has been successfully stored into the database. Edit Record or Go back to list')
        .should('be.visible')
      cy.wait(500)
      //Salva evidência de testes
      cy.screenshot('Evidência Desafio')
      
      //3. Fechar o browser (Retorna para tela inicial)
      cy.log('3. Fechar o browser (Retorna para tela inicial)')
      //cy.visit('https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap')
  
      cy.log('***Fim do Desafio 1***')

      //Início do Desafio 2
      cy.log('***Início do Desafio 2***')
      cy.wait(1000)

      //1. Clicar no link Go back to list
      cy.log('Go back to list')
      cy.contains('Go back to list')
        .should('be.visible').click({force:true})

      //2. Clicar na coluna “Search Name” e digitar o conteúdo do Name (Teste Sicredi)
      cy.log('2. Clicar na coluna “Search Name” e digitar o conteúdo do Name (Teste Sicredi)')
      cy.xpath('//*[@id="gcrud-search-form"]/div[2]/table/thead/tr[2]/td[3]/input')
        .click({force:true})
        .clear()
        .type('Teste Sicredi')
      cy.wait(2000)

      //3. Clicar no checkbox abaixo da palavra Actions
      cy.log('//3. Clicar no checkbox abaixo da palavra Actions')
      cy.xpath('//*[@id="gcrud-search-form"]/div[2]/table/thead/tr[2]/td[1]/div/input')
        .first()
        .click({force:true})
 
      //4. Clicar no botão Delete
      cy.log('4. Clicar no botão Delete')
      cy.contains('Delete')
        .should('be.visible')
        .click({force:true})

      //5. Validar o texto “Are you sure that you want to delete this 1 item?” através de uma asserção para a popup que será apresentada
      cy.log('//5. Validar o texto “Are you sure that you want to delete this 1 item?” através de uma asserção para a popup que será apresentada')
      cy.contains('Are you sure that you want to delete this 1 item?')
        .should('be.visible')
      cy.screenshot('Are you sure that you want to delete this 1 item')
      
      //6. Clicar no botão Delete da popup, aparecerá uma mensagem dentro de um box verde na parte superior direito da tela. Adicione uma asserção na mensagem “Your data has been successfully deleted from the database.”
      cy.log('//6. Clicar no botão Delete da popup, aparecerá uma mensagem dentro de um box verde na parte superior direito da tela. Adicione uma asserção na mensagem “Your data has been successfully deleted from the database.”')
      cy.xpath('/html/body/div[2]/div[2]/div[3]/div/div/div[3]/button[2]')
        .click({force:true})
      cy.screenshot('Your data has been successfully deleted from the database')
      cy.contains('Your data has been successfully deleted from the database.')
        .should('be.visible')

      //7. Fechar o browser
      cy.visit('https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap')

      cy.log('***Fim do Desafio 2***')

  })
})